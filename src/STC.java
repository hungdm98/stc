import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Queue;
import javax.swing.*;
import javax.swing.plaf.SliderUI;

import java.lang.Iterable;

public class STC {
    public static JFrame myFrame;

    public static void main(String[] args) {
        int width = 693;
        int height = 545;
        
        ImageIcon icon = new ImageIcon(STC.class.getResource("icon.png"));

        myFrame = new JFrame("STC Algorithm");
        myFrame.setIconImage(icon.getImage());
        myFrame.setContentPane((new Map(width, height)));
        myFrame.pack();
        myFrame.setResizable(false);

        // Center
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenWidth = screenSize.getWidth();
        double screenHeight = screenSize.getHeight();
        int x = ((int) screenWidth - width) / 2;
        int y = ((int) screenHeight - height) / 2;

        myFrame.setLocation(x, y);
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setBackground(Color.WHITE);
        myFrame.setVisible(true);
    }
    
    public static class Map extends JPanel {

        /**
         * Nested class for Helper
         */

        public static class Cell {
            public int row;
            public int col;

            public Cell(int row, int col) {
                this.row = row;
                this.col = col;
            }

            public String toString() {
                return " " + row + " " + col + " ";
            }
        }

        public class Edge {
            public Cell from;
            public Cell to;

            public Edge(Cell x, Cell y) {
                this.from = x;
                this.to = y;
            }

            public String toString() {
                return " " + this.from.toString() + " " + this.to.toString() + " ";
            }
        }
        
        
      class Description extends JLabel {
    	public Description() {
    		super();
    	}
    	public Description(String text) {
			// TODO Auto-generated constructor stub
    		super(text, JLabel.LEFT);
    		Color dark = new Color(66, 76, 88);
            Font font = new Font("Google sans", Font.PLAIN, 15);
            setForeground(dark);
            setFont(font);
		}   
    	public Description(String text, int size) {
    		super(text, JLabel.LEFT);
    		if (size > 0) {
        		Color dark = new Color(66, 76, 88);
                Font font = new Font("Google sans", Font.BOLD, size);
                setForeground(dark);
                setFont(font);
    		} else {
    			Color dark = new Color(66, 76, 88);
                Font font = new Font("Google sans", Font.ITALIC, -size);
                setForeground(dark);
                setFont(font);
    		}
    		
    	}
      }
      
      // Vẽ đồ thị minh họa
      class PieChart extends JComponent {
    	  class Slice {
          	  double value;
          	  Color color;

          	  public Slice(double value, Color color) {
          	    this.value = value;
          	    this.color = color;
          	  }
          	}
    	  
      	  Slice[] slices;
      	  
      	  PieChart() {

      	  }
      	  PieChart(int n, int val[], int obs, int emp) {
      		  this.slices = new Slice[n+2];
      		  for (int i = 0; i < n; i++) {
      			  slices[i] = new Slice(val[i], color[i]);
      		  }
      		  slices[n] = new Slice(obs, color_obs);
      		  slices[n+1] = new Slice(emp, color_empty);
      	  }
      	  
      	  public void paint(Graphics g) {
      	    drawPie((Graphics2D) g, getBounds(), slices);
      	  }

      	  void drawPie(Graphics2D g, Rectangle area, Slice[] slices) {
      	    double total = 0.0D;
      	    for (int i = 0; i < slices.length; i++) {
      	      total += slices[i].value;
      	    }

      	    double curValue = total/4;
      	    int startAngle = 0;
      	    for (int i = slices.length - 1; i >= 0; i--) {
      	      startAngle = (int) (curValue * 360 / total);
      	      int arcAngle = (int) (slices[i].value * 360 / total);

      	      g.setColor(slices[i].color);
      	      g.fillArc(area.x, area.y, area.width-15, area.height-15, startAngle, arcAngle);
      	      curValue += slices[i].value;
      	    }
      	  }
      	}
      
        private class MouseHandler implements MouseListener, MouseMotionListener {
            private int cur_row, cur_col, cur_val;

            @Override
            public void mousePressed(MouseEvent evt) {
                int row = (evt.getY() - 10) / squareSize;
                int col = (evt.getX() - 10) / squareSize;
                //System.out.println(row + " + " + col);
                if (row >= 0 && row < rows && col >= 0 && col < colums && !searching && !endOfSearch) {
                    cur_row = row;
                    cur_col = col;
                    cur_val = grid[row][col];
                    if (cur_val == EMPTY) {
                        grid[row][col] = OBST;
                    }
                    if (cur_val == OBST) {
                        grid[row][col] = EMPTY;
                    }
                }
                repaint();
            }

            @Override
            public void mouseDragged(MouseEvent evt) {
                int row = (evt.getY() - 10) / squareSize;
                int col = (evt.getX() - 10) / squareSize;
                System.out.println("Dragged at " + row + " " + col);
                if (row >= 0 && row < rows && col >= 0 && col < colums && !searching && !endOfSearch) {
                    if ((row * colums + col != cur_row * colums + cur_col) && (cur_val == ROBOT)) {
                        int new_val = grid[row][col];
                        if (new_val == EMPTY) {
                            grid[row][col] = cur_val;
                            if (cur_val == ROBOT) {
                            	for(int i = 0; i < 4; i++) {
                            		if(cur_col == robotStart[i].col && cur_row == robotStart[i].row) {
                            			robotStart[i].row = row;
                                        robotStart[i].col = col;
                            		}
                            	}                                
                            }
                            grid[cur_row][cur_col] = new_val;
                            cur_row = row;
                            cur_col = col;
//                            if (cur_val == ROBOT) {
//                                robotStart[0].row = cur_row;
//                                robotStart[0].col = cur_col;
//                            }
                            cur_val = grid[row][col];
                        }
                    } else if (grid[row][col] != ROBOT) {
                        grid[row][col] = OBST;
                    }
                }
                repaint();
            }


            @Override
            public void mouseReleased(MouseEvent evt) {
            }

            @Override
            public void mouseEntered(MouseEvent evt) {
            }

            @Override
            public void mouseExited(MouseEvent evt) {
            }

            @Override
            public void mouseClicked(MouseEvent evt) {
            }

            @Override
            public void mouseMoved(MouseEvent evt) {
            }
        }

        private class ActionHandler implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                if (cmd.equals("Clear")) {
                    fillGrid();
                } else if (cmd.equals("Animation") && !endOfSearch) {
                    if (!searching) {
                        initST();
                    }
                    message.setText("You can click 'Clear' when algorithm run completely");
                    searching = true;           // Find Spanning Tree
                    timer.setDelay(delay);
                    timer.start();
                    System.out.println("Start timer");
                    description.setText("Finding Spanning Tree");
                } else if (cmd.equals("Exit")) {
                    System.exit(0);
                }
            }
        }

        private class RepaintAction implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
            	// float time2 = 0.0f;
                // Draw spanning tree
                final Thread drawST = new Thread() {
                    int i = 0;

                    @Override
                    public void run() {
                    	int maxEdge = 0;
                    	for (int i=0; i<rbt; i++) {
                    		maxEdge = Math.max(maxEdge, E[i].size());
                    	}
                        for (int i=0; i<maxEdge; i++) {
                            //i++;
                            //i = i % 2;
                            if (i % 2 == 1) description.setText("Drawing ST ..   ");
                            else description.setText("Drawing ST .... ");

                            for (int j=0; j<rbt; j++) {
                            	if (i < E[j].size()) ST[j].add(E[j].get(i));
                            }
                            try {
                                sleep(100);
                                // time2 += 0.1f;
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                            repaint();
                        }
                    }
                };
                drawST.start();

                Thread showPath = new Thread() {
                    int i = 0;
                    
                    @Override
                    public void run() {
                        try {
                            drawST.join();

                        } catch (InterruptedException err) {
                            err.printStackTrace();
                        }
                        int maxStep = 0;
                        for (int i=0; i<rbt; i++) {
                        	maxStep = Math.max(maxStep, Path[i].size());
                        }
                        for (int i=0; i<maxStep; i++) {
                        	for (int j=0; j<rbt; j++) {
                        		if (i < Path[j].size()) {
                        			Cell c = Path[j].get(i);
                                	goPath[j].add(c);
                        		}
                        	}
                        	
                            //i++;
                            // i = i % 2;
                            if (i % 2 == 1) description.setText("Drawing Path ..   ");
                            else description.setText("Drawing Path .... ");

                            
                            try {
                                sleep(90);
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                            repaint();
                        }
                        description.setText("Draw Path Complete !");
                        
                        // Hiện hộp thoại thống kê kết quả
                        
                        // pathSize: độ dài đường đi của mỗi robot
                        int[] pathSize = new int[rbt];
                        int total = 0;
                        for (int i = 0; i < rbt; i++) {
                        	pathSize[i] = goPath[i].size()-1;
                        	total += pathSize[i];
                        }
                        
                        // obsSize: số lượng vật cản
                        int obsSize = 0;
                        for (int i = 0; i < rows; i++) {
                        	for (int j = 0; j < colums; j++) {
                        		if (grid[i][j] == 1) obsSize++;
                        	}
                        }
                        System.out.println(obsSize);
                        JFrame frame = new JFrame("Result");
                        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        //frame.setResizable(false);
                        frame.setSize(900, 600);
                        JPanel contentPane = new JPanel(){
                            @Override
                            public Dimension getPreferredSize() {
                                return new Dimension(540, 290);
                            };
                        };
                        //contentPane.setBounds(0, 0, 300, 300);
                        
                        contentPane.setLayout(null);
                        PieChart myComponent = new PieChart(rbt, pathSize, obsSize, rows*colums - obsSize - total);
                        myComponent.setSize(200, 200);
                        myComponent.setLocation(10, 10);
                        frame.setContentPane(contentPane);
                        
                        int percent = total * 100 / (rows * colums - obsSize); 
                        //JLabel description;
                        description = new Description("Tỉ lệ bao phủ: " + percent + "%", 25);
                        description.setBounds(240, 25, 400, 25);
                        frame.getContentPane().add(description);
                        
                        int curY = 70;
                        for (int i = 0; i < rbt; i++) {
                        	//Rectangle rect = new Rectangle();
                        	description = new Description(" ");
                        	description.setBackground(color[i]);
                        	description.setOpaque(true);
                        	//description.setForeground(color[i]);
                        	description.setBounds(240, curY, 15, 15);
                        	frame.getContentPane().add(description);
                        	//
                        	float time = 0.0f;
                        	description = new Description("Robot #" + (i+1) + ": ");
                            description.setBounds(270, curY, 400, 15);
                            //curY += 20;
                            frame.getContentPane().add(description);
                            description = new Description(pathSize[i] + " ô");
                            description.setBounds(340, curY, 400, 15);
                            //curY += 20;
                            frame.getContentPane().add(description);
                            //
                            time = E[i].size() * 0.1f + Path[i].size() * 0.09f;
                            description = new Description(time + "s", -15);
                            description.setBounds(410, curY, 400, 15);
                            frame.getContentPane().add(description);
                            curY += 20;
                        }
                        
                        // vật cản
                    	description = new Description(" ");
                    	description.setBackground(color_obs);
                    	description.setOpaque(true);
                    	//description.setForeground(color[i]);
                    	description.setBounds(240, curY, 15, 15);
                    	frame.getContentPane().add(description);
                    	//
                    	description = new Description("Vật cản: ");
                        description.setBounds(270, curY, 400, 15);
                        //curY += 20;
                        frame.getContentPane().add(description);
                        description = new Description(obsSize + " ô");
                        description.setBounds(340, curY, 400, 15);
                        frame.getContentPane().add(description);
                        curY += 20;
                        
                        // ô trống
                        description = new Description(" ");
                    	description.setBackground(color_empty);
                    	description.setOpaque(true);
                    	//description.setForeground(color[i]);
                    	description.setBounds(240, curY, 15, 15);
                    	frame.getContentPane().add(description);
                    	//
                    	description = new Description("Ô trống: ");
                        description.setBounds(270, curY, 400, 15);
                        //curY += 20;
                        frame.getContentPane().add(description);
                        description = new Description(rows*colums - (total + obsSize) + " ô");
                        description.setBounds(340, curY, 400, 15);
                        frame.getContentPane().add(description);
                        curY += 20;
                        
                        
                        // contentPane.add(description);
                        
                        frame.getContentPane().add(myComponent);
                        
                        frame.pack();   
                        frame.setLocationByPlatform(true);
                        
                        frame.setVisible(true);
                        
                        //frame.open
                    }
                };
                showPath.start();

                endOfSearch = true;
                if (endOfSearch) {
                    timer.stop();
                    System.out.println("Timer Stopped");
                }
                repaint();

            }
        }

        /**
         * Atribute of Map class
         */
//        ImageIcon icon0 =new ImageIcon(getClass().getClassLoader().getResource("step0.ico"));
//        Image img0=icon0.getImage();

        ImageIcon icon1 =new ImageIcon(getClass().getClassLoader().getResource("step1.png"));
        Image img1=icon1.getImage();

        ImageIcon icon2 =new ImageIcon(getClass().getClassLoader().getResource("step2.png"));
        Image img2=icon2.getImage();

        ImageIcon rock =new ImageIcon(getClass().getClassLoader().getResource("rock.png"));
        Image Rock = rock.getImage();
        final int rbt = 8;
		ImageIcon ico_step[] = new ImageIcon[rbt];
        ImageIcon ico_focus[] = new ImageIcon[rbt];
        Image img_step[] = new Image[rbt];
        Image img_focus[] = new Image[rbt];
        private final static int
                EMPTY = 0,              // State of cell
                OBST = 1,
                ROBOT = 2,
                VERTEX = 3,
                ROUTE = 4,              // Spanning Tree
                STEP = 5;               // Step of robot along with STree


        JLabel message;     // Message to user
        // Màu cho robot
        Color[] color = {new Color(0, 188, 212), new Color(255, 235, 59), new Color(139, 195, 74), new Color(156, 39, 176), new Color(233, 30, 99), new Color(63, 81, 181), new Color(255, 87, 34), new Color(205, 220, 57) };
        Color color_obs = new Color(96, 125, 139);
        Color color_empty = new Color(121, 85, 72);
        
        private final static String msgDraw = "Di chuyển Robot => Vẽ chướng ngại vật => nhấn 'Animation'";

        JLabel description;


        int
                rows = 20,
                colums = 20,
                squareSize = 500 / rows;

        int[][] grid;
        
        Cell robotStart[] = new Cell[rbt];
        //boolean found;              // Three boolean for find Spanning Tree
        boolean endOfSearch;          // Done Search
        boolean searching;            // Searching
        ArrayList<Cell> V[] = new ArrayList[rbt];  //Graph, danh sách các đỉnh của đồ thị
        ArrayList<Edge> E[] = new ArrayList[rbt];          // Danh sách cạnh của Cây Khung cần tìm
        ArrayList<Edge> ST[] = new ArrayList[rbt];          // Cây khung mà chương trình dựng ra
        ArrayList<Cell> Path[] = new ArrayList[rbt];        // Đường đi của robot
        ArrayList<Cell> goPath[] = new ArrayList[rbt];   // Đường đi của robot mà chương trình dựng ra
        boolean rightHand[] = new boolean[rbt];
        int delay;
        Timer timer;
        RepaintAction action = new RepaintAction();

        /**
         * Constructor of Map
         *
         * @param width
         * @param height
         */

        public Map(int width, int height) {
            setLayout(null);
            MouseHandler listener = new MouseHandler();
            addMouseListener(listener);
            addMouseMotionListener(listener);
            //setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, new Color(0, 0, 0)));
            setPreferredSize(new Dimension(width, height));

            grid = new int[rows][colums];

            Color dark = new Color(66, 76, 88);
            Color orange = new Color(246, 142, 79);
            Color cyan = new Color(42, 198, 220);
            Color green = new Color(35, 181, 116);
            //String font = "Google sans";
            Font font = new Font("Google sans", Font.PLAIN, 15);
            
            description = new JLabel("Initializing ...", JLabel.CENTER);
            description.setForeground(dark);
            description.setFont(font);
            message = new JLabel(msgDraw, JLabel.CENTER);
            message.setForeground(dark);
            message.setFont(font);

            JButton statusButton = new JButton("Status");
            statusButton.setBackground(dark);
            statusButton.setFont(font);
            statusButton.setBorder(null);
            statusButton.setForeground(Color.WHITE);
            statusButton.setEnabled(false);

            JButton clearButton = new JButton("Clear");
            clearButton.addActionListener(new ActionHandler());
            clearButton.setBackground(dark);
            clearButton.setForeground(Color.WHITE);
            clearButton.setFont(font);
            clearButton.setBorder(null);
            clearButton.setToolTipText("Nhấn lần đầu để xóa kết quả tìm kiếm. Nhấn lần hai để xóa chướng ngại vật");

            JButton animationButton = new JButton("Animation");
            animationButton.addActionListener(new ActionHandler());
            animationButton.setBackground(dark);
            animationButton.setForeground(Color.WHITE);
            animationButton.setFont(font);
            animationButton.setBorder(null);
            animationButton.setToolTipText("Nhấn vào đây để tiến hành quá trình chạy thuật toán STC");

            JButton exitButton = new JButton("Exit");
            exitButton.addActionListener(new ActionHandler());
            exitButton.setBackground(new Color(232, 76, 61));
            exitButton.setForeground(Color.WHITE);
            exitButton.setFont(font);
            exitButton.setBorder(null);
            exitButton.setToolTipText("Tắt chương trình");


            add(description);
            add(message);
            add(statusButton);
            add(clearButton);
            add(exitButton);
            add(animationButton);

            message.setBounds(0, 515, 500, 23);
            clearButton.setBounds(525, 20, 150, 50);
            animationButton.setBounds(525, 80, 150, 50);
            statusButton.setBounds(525, 140, 150, 50);
            description.setBounds(525, 220, 170, 50);
            exitButton.setBounds(525, 455, 150, 50);


            // Thêm mấy text chỉ dẫn màu mè : Robot -> Red, Way -> bla bla bla


            delay = 3;
            timer = new Timer(delay, action);

            // Clear at start of program
            for (int i=0; i<rbt; i++) {
            	ST[i] = new ArrayList<>();
            	Path[i] = new ArrayList<>();
            	goPath[i] = new ArrayList<>();
            	V[i] = new ArrayList<>();
        	    E[i] = new ArrayList<>();
        	    // Load images
        	    ico_step[i] =new ImageIcon(getClass().getClassLoader().getResource("step"+(i+1)+".png"));
                img_step[i] = ico_step[i].getImage();
                ico_focus[i] =new ImageIcon(getClass().getClassLoader().getResource("focus"+(i+1)+".png"));
                img_focus[i] = ico_focus[i].getImage();
            }
            
            fillGrid();
        }

        /**
         * Clear the Grid map
         */
        private void fillGrid() {
        	
            // First Click CLEAR
            if (searching || endOfSearch) {
            	int d = 0;
                for (int r = 0; r < rows; r++) {
                    for (int c = 0; c < colums; c++) {
                        if (grid[r][c] == ROBOT) {
                            robotStart[d] = new Cell(r, c);
                            d++;
                        } else if (grid[r][c] == VERTEX || grid[r][c] == ROUTE || grid[r][c] == STEP) {
                            grid[r][c] = EMPTY;
                        }
                    }
                    searching = false;
                }
            }
            // Second click
            else {
                for (int r = 0; r < rows; r++) {
                    for (int c = 0; c < colums; c++) {
                        grid[r][c] = EMPTY;
                    }
                }
                robotStart[0] = new Cell(1, 1);
                robotStart[1] = new Cell(1, 9);
                robotStart[2] = new Cell(9, 1);
                robotStart[3] = new Cell(9, 9);
                
                robotStart[4] = new Cell(11, 9);
                robotStart[5] = new Cell(13, 9);
                robotStart[6] = new Cell(15, 9);
                robotStart[7] = new Cell(17, 9);
            }

            searching = false;
            endOfSearch = false;
            for (int i=0; i<rbt; i++) {
            	rightHand[i] = false;
            }
            
            for (int i = 0; i < rbt; i++) {
            	grid[robotStart[i].row][robotStart[i].col] = ROBOT;
            }
            for(int i = 0; i < rbt; i++) {                
                ST[i].removeAll(ST[i]);
                goPath[i].removeAll(goPath[i]);
            }
            message.setText(msgDraw);
            timer.stop();
            repaint();
        }

        // Danh sách các đỉnh kề với nó trong đồ thị
        private ArrayList<Cell> createSuccesors(Cell current) {
            int r = current.row;
            int c = current.col;
            ArrayList<Cell> temp = new ArrayList<Cell>();

//            if (r > 1 && r % 2 == 1 && grid[r - 2][c] != OBST) {
//                Cell cell = new Cell(r - 2, c);
//                temp.add(cell);
//            }
//            if (r < rows - 2 && r % 2 == 1 && grid[r + 2][c] != OBST) {
//                Cell cell = new Cell(r + 2, c);
//                temp.add(cell);
//            }
//            if (c > 1 && c % 2 == 1 && grid[r][c - 2] != OBST) {
//                Cell cell = new Cell(r, c - 2);
//                temp.add(cell);
//            }
//            if (c < colums - 2 && c % 2 == 1 && grid[r][c + 2] != OBST) {
//                Cell cell = new Cell(r, c + 2);
//                temp.add(cell);
//            }
            // Bac
            if (r > 2 && c > 0  && grid[r - 2][c] != OBST && grid[r - 3][c] != OBST && grid[r - 2][c-1] != OBST && grid[r - 3][c-1] != OBST) {
                Cell cell = new Cell(r - 2, c);
                temp.add(cell);
            }
            // Nam
            if (r < (rows - 2) && c > 0 && grid[r + 1][c] != OBST && grid[r + 2][c] != OBST && grid[r + 1][c-1] != OBST && grid[r + 2][c-1] != OBST) {
                Cell cell = new Cell(r + 2, c);
                temp.add(cell);
            }
            // Dong
            if (c > 1 && r > 0 && grid[r][c - 2] != OBST && grid[r][c - 3] != OBST && grid[r-1][c - 2] != OBST && grid[r-1][c - 3] != OBST) {
                Cell cell = new Cell(r, c - 2);
                temp.add(cell);
            }
            // Tay
            if (c < colums -2 && r > 0 && grid[r][c + 1] != OBST && grid[r][c + 2] != OBST && grid[r-1][c + 1] != OBST && grid[r-1][c + 2] != OBST) {
                Cell cell = new Cell(r, c + 2);
                temp.add(cell);
            }
            return temp;
        }


        private int isInList(ArrayList<Cell> list, Cell current) {
            int index = -1;
            for (int i = 0; i < list.size(); i++) {
                if (current.row == list.get(i).row && current.col == list.get(i).col) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        // Dựng đồ thị (các đỉnh thuộc thành phần liên thông với đỉnh Start), dùng Deapth First Search
        // Đồng thời dựng cây khung , vì DFS ouput ra một cây khung của đồ thị
        private void findConnectedComponent(Cell v[], int n) {
            Queue<Cell> queue[] = new Queue[n];
            for (int i=0; i<n; i++) {
            	queue[i] = new LinkedList<>();
            	queue[i].add(v[i]);
            	V[0].add(v[i]);
            }
            
            ArrayList<Cell> succesors = new ArrayList<>();
            boolean cont = true;
            // V[0].add(v);
            while (cont) {
            	cont = false;
            	boolean selectRand[] = new boolean[n+1];
            	for (int i=0; i<n; i++) {
            		selectRand[i] = true;
            	}
            	selectRand[n] = false;
            	for (int i=0; i<n; i++) {
            		int j = n;
            		while (!selectRand[j]) {
            			j = (int) (Math.random()*1000);
                		j %= n;
                		System.out.println(j);
            		}
            		selectRand[j] = false;
            		if (!queue[j].isEmpty()) {
            			cont = true;
            			Cell x = queue[j].poll();
            			succesors = createSuccesors(x);
            			for (Cell c : succesors) {
                            if (isInList(V[0], c) == -1) {
                                queue[j].add(c);
                                V[0].add(c);
                                E[j].add(new Edge(x, c));
                            }
                        }
            		}
            	}
            	
            }
            System.out.println(V[0].size() + " vertex available of Graph");
            for (Cell c : V[0]) {
                System.out.println(c.row + " " + c.col);
                grid[c.row][c.col] = VERTEX;
                try {
                    Thread.sleep(2);
                } catch (InterruptedException err) {
                    err.printStackTrace();
                }
                repaint();
            }
            grid[robotStart[0].row][robotStart[0].col] = ROBOT;
            grid[robotStart[1].row][robotStart[1].col] = ROBOT;
            grid[robotStart[2].row][robotStart[2].col] = ROBOT;
            grid[robotStart[3].row][robotStart[3].col] = ROBOT;
            System.out.println("We have " + E[0].size() + " edge");
            for (int i=0; i<rbt; i++) {
            	for (int j = 0; j < E[i].size(); j++) {
                    Cell from = E[i].get(j).from;
                    Cell to = E[i].get(j).to;
                    if (from.row == robotStart[i].row && from.col == robotStart[i].col && robotStart[i].col == to.col && robotStart[i].row < to.row) rightHand[i] = true;
                    System.out.println(from.toString() + "=> " + to.toString());
                }
            }
            
        }

        // Tìm Spanning Tree
        private void initST() {
        	// clear các mảng tương ứng
        	for (int i=0; i<rbt; i++) {
        		V[i].removeAll(V[i]);
                E[i].removeAll(E[i]);
                ST[i].removeAll(ST[i]);
                Path[i].removeAll(Path[i]);
                goPath[i].removeAll(goPath[i]);
        	}
            
//            robotStart[1] = new Cell(1, 9);
//            robotStart[2] = new Cell(9, 1);
//            robotStart[3] = new Cell(9, 9);
            findConnectedComponent(robotStart, rbt);
            
            for (int i=0; i<rbt; i++) {
            	Path[i] = wallFollow(robotStart[i], E[i], rightHand[i]);
        	}
            
        }

        /**
        - Sau khi tìm được Spanning Tree, ta có một mê cung (maze).
            Dùng thuật toán Wall Follower (cụ thể là quy tắc bàn tay phải) để tính toán các ô cần đi
            Ban đầu mặc định Robot quay đầu về hướng Nam (thật ra hướng nào cũng okie). Ô hiện tại là ô RobotStart
            Ở mỗi bước lặp, ta liệt kê danh sách 4 neighbour của ô hiện tại (r,c) là (r-1,c),(r+1,c),(r,c-1),(r,c+1)
            Một ô có thể đến được từ ô hiện tại nếu :
            Chúng thuộc cùng một big-Cell (2x2)
            Hoặc thuộc 2 big-Cell khác nhau, nhưng tâm điểm của 2 big-Cell này tạo thành 1 edge của Spanning Tree
        - Xác định ô kế tiếp để đi
            Nếu có thể rẽ phải => Rẽ phải
            Nếu không, Nếu có thể đi thẳng => Đi thẳng
            Nếu không, Nếu có thể rẽ trái => Rẽ trái
            Các hướng thẳng, trái, phải xác định dựa trên một biến Direction, và vị trí của ô neighbour so với ô hiện tại
        - Quá trình kết thúc khi gặp lại ô ban đầu. Lưu kq trong temp. Temp sẽ có > 2x(E.size()) node.

         */

        //
        boolean onSegment(Cell p, Cell q, Cell r) {
            if (q.row <= Math.max(p.row, r.row) && q.row >= Math.min(p.row, r.row) &&
                    q.col <= Math.max(p.col, r.col) && q.col >= Math.min(p.col, r.col))
                return true;

            return false;
        }

        // Hướng của tam giác (p,q,r)
        int orientation(Cell p, Cell q, Cell r) {
            int val = (q.col - p.col) * (r.row - q.row) -
                    (q.row - p.row) * (r.col - q.col);

            if (val == 0) return 0;  // colinear

            return (val > 0) ? 1 : 2; // clock or counterclock wise
        }

        boolean isIntersect(Cell p1, Cell q1, Cell p2, Cell q2) {
            // Find the four orientations needed for general and
            // special cases
            int o1 = orientation(p1, q1, p2);
            int o2 = orientation(p1, q1, q2);
            int o3 = orientation(p2, q2, p1);
            int o4 = orientation(p2, q2, q1);

            // General case
            if (o1 != o2 && o3 != o4)
                return true;


            // Special Cases
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1
            if (o1 != 0 && onSegment(p1, p2, q1)) return true;

            // p1, q1 and p2 are colinear and q2 lies on segment p1q1
            if (o2 != 0 && onSegment(p1, q2, q1)) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2
            if (o3 != 0 && onSegment(p2, p1, q2)) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2
            if (o4 != 0 && onSegment(p2, q1, q2)) return true;

            return false; // Doesn't fall in any of the above cases
        }

        boolean canGo(Cell X, Cell Y, int Ori, ArrayList<Edge> El) {
            int Xr = X.row;
            int Xc = X.col;
            int Yr = Y.row;
            int Yc = Y.col;
            // X là ô hiện tại, chắc chắn OK
            // Y là ô kế tiếp, Cần kiểm tra các bước, nếu okie hết thì return true

            // 1. Thuộc map
            if (Yr < 0 || Yr > rows - 1 || Yc < 0 || Yc > colums - 1) return false;

            // 2. Chung cạnh với X (==r || ==c)
            // -> Cái này hiển nhiên là true do cách chọn ô Y

            // 3. Không là ô vật cản dạng big-Cell (OBST)
            if (Yr % 2 == 1 && Yc % 2 == 1 && grid[Yr][Yc] == OBST) return false;
            if (Yr % 2 == 0 && Yc % 2 == 0 && grid[Yr + 1][Yc + 1] == OBST) return false;
            if (Yr % 2 == 0 && Yc % 2 == 1 && grid[Yr + 1][Yc] == OBST) return false;
            if (Yr % 2 == 1 && Yc % 2 == 0 && grid[Yr][Yc + 1] == OBST) return false;

            // 4. Không bị một cạnh nào của Spanning Tree (Wall) chắn. Phức tạp =.=
            for (Edge e : El) {
                if (!isIntersect(X, Y, e.from, e.to)) continue;
                else {
                    if (Ori == 1) {
                        if (Y.col == e.from.col && e.from.col == e.to.col && Y.row != Math.max(e.from.row, e.to.row)){
                            return false;
                        }
                    }
                    if (Ori == 2) {
                        if (X.col == e.from.col && e.from.col == e.to.col && X.row != Math.max(e.from.row, e.to.row)) {
                            return false;
                        }
                    }
                    if (Ori == 3) {
                        if (X.row == e.from.row && e.from.row == e.to.row && X.col != Math.max(e.from.col, e.to.col)) {
                            return false;
                        }
                    }
                    if (Ori == 4) {
                        if (Y.row == e.from.row && e.from.row == e.to.row && Y.col != Math.max(e.from.col, e.to.col)) {
                            return false;
                        }
                    }
                }

            }
            return true;
        }

        private ArrayList<Cell> wallFollow(Cell robotStart, ArrayList<Edge> El, boolean rightHand) {
            ArrayList<Cell> temp = new ArrayList<Cell>();
            Cell curCell = robotStart;
            temp.add(curCell);
            int curDir = 1;           // 1,2,3,4. Tương đương với việc hướng mặt về 4 phía 1-Đông, 2-Tây, 3-Bắc, 4-Nam
            int r, c;
            Cell R, S, L;
            int i = 0;

            if (rightHand) {
                do {
                    System.out.println("Right Hand Algorithm");
                    i++;
                    r = curCell.row;
                    c = curCell.col;

                    if (curDir == 1) {
                        // Xét xem nếu có thể rẽ phải
                        R = new Cell(r + 1, c);
                        S = new Cell(r, c + 1);
                        L = new Cell(r - 1, c);
                        if (canGo(curCell, R, 4, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 4;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 1, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 1;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, L, 3, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 3;
                        }

                    } else if (curDir == 2) {
                        R = new Cell(r - 1, c);
                        S = new Cell(r, c - 1);
                        L = new Cell(r + 1, c);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, R, 3, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 3;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 2, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 2;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, L, 4, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 4;
                        }
                    } else if (curDir == 3) {
                        R = new Cell(r, c + 1);
                        S = new Cell(r - 1, c);
                        L = new Cell(r, c - 1);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, R, 1, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 1;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 3, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 3;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, L, 2, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 2;
                        }
                    } else {
                        R = new Cell(r, c - 1);
                        S = new Cell(r + 1, c);
                        L = new Cell(r, c + 1);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, R, 2, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 2;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 4, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 4;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, L, 1, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 1;
                        }
                    }
                    System.out.println(curCell + " " + curDir);
                    //if (i >= 500) break;
                } while (curCell.row != robotStart.row || curCell.col != robotStart.col);
            } else {
                System.out.println("Left Hand Algorithm");
                do {
                    i++;
                    r = curCell.row;
                    c = curCell.col;

                    if (curDir == 1) {
                        // Xét xem nếu có thể rẽ phải
                        R = new Cell(r + 1, c);
                        S = new Cell(r, c + 1);
                        L = new Cell(r - 1, c);
                        if (canGo(curCell, L, 3, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 3;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 1, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 1;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, R, 4, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 4;
                        }

                    } else if (curDir == 2) {
                        R = new Cell(r - 1, c);
                        S = new Cell(r, c - 1);
                        L = new Cell(r + 1, c);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, L, 4, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 4;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 2, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 2;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, R, 3, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 3;
                        }
                    } else if (curDir == 3) {
                        R = new Cell(r, c + 1);
                        S = new Cell(r - 1, c);
                        L = new Cell(r, c - 1);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, L, 2, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 2;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 3, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 3;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, R, 1, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 1;
                        }
                    } else {
                        R = new Cell(r, c - 1);
                        S = new Cell(r + 1, c);
                        L = new Cell(r, c + 1);
                        // Xét xem nếu có thể rẽ phải
                        if (canGo(curCell, L, 1, El)) {
                            curCell = L;
                            temp.add(L);
                            curDir = 1;
                        }
                        // Không thể rẽ phải, xét xem nếu có thể đi thẳng
                        else if (canGo(curCell, S, 4, El)) {
                            curCell = S;
                            temp.add(S);
                            curDir = 4;
                        }
                        // Không thể rẽ phải, đi thẳng, xét xem nếu có thể rẽ trái
                        else if (canGo(curCell, R, 2, El)) {
                            curCell = R;
                            temp.add(R);
                            curDir = 2;
                        }
                    }
                    System.out.println(curCell + " " + curDir);
                    //if (i >= 500) break;
                } while (curCell.row != robotStart.row || curCell.col != robotStart.col);
            }

            System.out.println("We have " + temp.size() + " cells in actully Path");
            for (Cell cc : temp) {
                System.out.println(cc.toString());
            }
            return temp;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.DARK_GRAY);
            g.fillRect(10, 10, colums * squareSize + 1, rows * squareSize + 1);
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < colums; c++) {
                    if (grid[r][c] == EMPTY) {
                        g.setColor(Color.WHITE);
                        g.fillRect(11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1);
                    } else if (grid[r][c] == VERTEX) {
                        g.setColor(Color.WHITE);
                        g.fillRect(11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1);
                        g.setColor(Color.GREEN);
                        /*
                        try {
                            Thread.sleep(7);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        */
                        g.fillOval(4 + c * squareSize, 4 + r * squareSize, 10, 12);
                    } else if (grid[r][c] == ROBOT) {
                        g.setColor(Color.WHITE);
                        g.fillRect(11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1);
                        g.setColor(Color.RED);
                        g.fillOval(2 + c * squareSize, 2 + r * squareSize, 16, 16);
                        //g.fillRect(13 + c * squareSize, 13 + r * squareSize, squareSize - 5, squareSize - 5);
                    } else if (grid[r][c] == OBST) {
                        //g.setColor(Color.BLACK);
                        //g.fillRect(11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1);
                        g.drawImage(Rock,11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1,null);
                    }
                }
            }
            g.setColor(Color.BLUE);
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < colums; c++)
                    if (r % 2 == 0 && c % 2 == 0) {
                        g.drawRect(11 + c * squareSize, 11 + r * squareSize, 2 * squareSize - 1, 2 * squareSize - 1);
                    }
            }
            g.setColor(Color.darkGray);
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < colums; c++)
                    if (r % 2 == 1 && c % 2 == 1 && grid[r][c] != VERTEX && grid[r][c] != ROBOT) {
                        g.fillOval(6 + c * squareSize, 6 + r * squareSize, 8, 10);
                    }
            }

            int step=0;
            int maxStep = 0;
            for (int i=0; i<rbt; i++) {
            	maxStep = Math.max(maxStep, goPath[i].size());
            }
            for(int i=0;i<maxStep;i++) {
                step++;
                for (int j=0; j<rbt; j++) {
                	if (i < goPath[j].size() ) {
                		Cell cell=goPath[j].get(i);
                        int r = cell.row;
                        int c = cell.col;
                        if(step==goPath[j].size()){
                            g.drawImage(img_focus[j],11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1,null);
                        }
                        else g.drawImage(img_step[j],11 + c * squareSize, 11 + r * squareSize, squareSize - 1, squareSize - 1,null);
                	}
                }

            }

            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(new Color(244, 67, 54));
            g2d.setStroke(new BasicStroke(3));
            int maxEdge = 0;
            for (int i=0; i<rbt; i++) {
            	maxEdge = Math.max(maxEdge, ST[i].size());
            }
            for(int i=0;i<maxEdge;i++) {
            	for (int j=0; j<rbt; j++) {
            		if (i < E[j].size()) {
            			Edge e=E[j].get(i);
                        Cell from = e.from;
                        Cell to = e.to;
                        //System.out.println(from.toString() + "=> " + to.toString());
                        g2d.drawLine(10 + from.col * squareSize, 10 + from.row * squareSize, 10 + to.col * squareSize, 10 + to.row * squareSize);
            		}
            	}
            	
            }


        }
    }
}